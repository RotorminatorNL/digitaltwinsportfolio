﻿import { createWebHistory, createRouter } from "vue-router";
import Home from "@/components/Home.vue";
import Product from "@/components/Product.vue";
import Problem from "@/components/Problem.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/Product/:productId",
        name: "Product",
        component: Product,
    },
    {
        path: "/Product/:productId/Problem/:problemId",
        name: "Problem",
        component: Problem,
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;